import IPython
from IPython.display import Image
from IPython.display import Audio

class Media:
    def __init__(self, audio_url, image_url):
        self.audio = Image(url=image_url)
        self.img = Audio(url=audio_url, autoplay=True)
    
    def display(self):
        IPython.display.display(self.img)
        IPython.display.display(self.audio)
def morgen_play():
    morgen = Media(
        'https://mp3uks.ru/mp3/files/morgenshtern-show-mp3.mp3',
        'https://c.tenor.com/rCChRXMUdvsAAAAC/%D0%BC%D0%BE%D1%80%D0%B3%D0%B5%D0%BD%D1%88%D1%82%D0%B5%D1%80%D0%BD-%D0%BA%D0%BB%D0%BE%D1%83%D0%BD.gif'
    )

    morgen.display()
