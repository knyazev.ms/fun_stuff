import numpy as np
import IPython
from IPython.display import Image
from IPython.display import Audio

def gandalf_says(text):
    print('Gandalf:')
    print('\t', text)
    img = Image(url='https://i.gifer.com/origin/51/51f1ea94effbb251598e67e8522e58f5.gif')
    IPython.display.display(img)
    mp3_url = 'https://dl.mp3home.me/download/7493/Sunstroke-project-Olia-Tira_-_Run-Away-Moldova_(mp3home.me).mp3'
    mp3 = Audio(url=mp3_url, autoplay=True)
    IPython.display.display(mp3)

if __name__ == '__main__':
    gandalf_says('Hello world!!!')
